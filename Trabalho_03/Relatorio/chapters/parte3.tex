\chapter{Parte 3}
\label{cap:p3}

\section{Alterações ao Modelo}

Visto esta parte ter como base a parte V do 1ª trabalho prático, no presente relatório dá-se ênfase às alterações feitas ao modelo desse trabalho e não à caracterização exaustiva do mesmo. Essa caracterização mais detalhada pode ser encontrada no relatório do 1º trabalho.

\subsection{Alterações às variáveis de decisão}
Todas as variáveis presentes no modelo da parte V do trabalho 1 foram mantidas. Foi no entanto necessário adicionar novas variáveis binárias, $B_i$ que para uma atividade $i$ indicam se a redução do tipo 2 pode ser realizada para essa atividade. Esta variável apenas tomará o valor 1 caso a redução do tipo 1 (a mais cara) atinga o seu valor máximo, e toma o valor 0 caso contrário.

\subsection{Alterações à função objectivo}

No trabalho 1 foi tomada a decisão da função objectivo representar apenas o custo adicional que as reduções provocariam. Por uma questão de simplificar a posterior análise dos resultados, neste trabalho decidiu-se fazer uma pequena modificação de modo a que a função objectivo passe agora a traduzir o custo total do projeto e não apenas o custo adicional das reduções. Isto implicou adicionar os custos das atividades normais à função objectivo. Uma vez que neste trabalho os valores dos custos das reduções são diferentes dos do 1º trabalho, a função objectivo também teve que ser ajustada em conformidade.

Para uma atividade $i$, temos que o seu custo total $C_i$ (contando com as eventuais reduções) pode ser dado por:

\begin{displaymath}
C_{i} = CN_i  + (C_{i\_1} \times R_{i\_1} + C_{i\_2} \times R_{i\_2})
\end{displaymath}

Onde:

\begin{itemize}
	\item[$CN_i$] Custo normal da atividade i
	\item[$C_{i\_1}$] Custo por unidade de tempo associada a reduções do tipo 1 na duração da atividade $i$
	\item[$R_{i\_1}$] Unidades de tempo reduzir à duração da atividade $i$ a custo $C_{i\_1}$
	\item[$C_{i\_2}$] Custo por unidade de tempo associada a reduções do tipo 2 na duração da atividade $i$
	\item[$R_{i\_2}$] Unidades de tempo reduzir à duração da atividade $i$ a custo $C_{i\_2}$
\end{itemize}

A função objetivo, $z$, corresponde ao somatório dos custos de redução de todas as atividades, ou seja:

\begin{displaymath}
min~z = \sum C_i
\end{displaymath}

A função objectivo completa com os valores dos parâmetros devidamente substituídos pode ser consultada na secção \ref{p3:fichin} onde se apresenta o ficheiro de input.

\subsection{Alterações às restrições}

Recorde-se que do trabalho 1, as restrições existentes visavam garantir que os valores das reduções não ultrapassavam o máximo permitido. Assim, para cada atividade $i$ tinha-se duas restrições:

\begin{equation}
\label{p3:eq1}
R_{i\_1} \leq Rmax_{i\_1}
\end{equation}
\begin{equation}
\label{p3:eq2}
R_{i\_2} \leq Rmax_{i\_2}
\end{equation}

Onde:

\begin{itemize}
	\item[$R_{i\_1}$] Variável de decisão --- redução (em unidades de tempo) do tipo 1 a aplicar à atividade $i$
	\item[$R_{i\_2}$] Variável de decisão --- redução (em unidades de tempo) do tipo 2 a aplicar à atividade $i$
	\item[$Rmax_{i\_1}$] Redução máxima de tempo do tipo 1 que é possível aplicar à atividade $i$
	\item[$Rmax_{i\_2}$] Redução máxima de tempo do tipo 2 que é possível aplicar à atividade $i$
\end{itemize}

No trabalho 1 estas restrições eram suficientes, pois o custo da redução 1 em todas as atividades era sempre menor que o custo da redução 2. Visto que se trata de um modelo de minimização, o lpsolve naturalmente iria sempre escolher primeiro aplicar a redução 1 e só depois a redução 2. Isto não acontece neste trabalho: a redução 2 é mais barata que a redução 1 e quer-se forçar que se aplica primeiro a redução 1 e só depois a 2.

Em primeiro lugar, tem que se garantir que se a redução 2 não puder ser aplicada, então o seu valor não pode ser maior que zero. Por outro lado, caso a redução 2 possa ser aplicada, quer-se que essa redução seja limitada pelo valor máximo dado como parâmetro. Visto que a variável $B_i$ indica se a redução 2 pode ou não ser aplicada à atividade $i$, então incorporar no modelo tal requisito consiste em alterar a restrição dada pela equação \ref{p3:eq2} multiplicando o valor da variável binária pelo valor da redução, obtendo-se então a restrição \ref{p3:eq3}:

\begin{equation}
\label{p3:eq3}
R_{i\_2} \leq Rmax_{i\_2} \times B_i
\end{equation}

Note-se que caso a redução 2 não possa ser aplicada à atividade $i$, tem-se que $B_i = 0 \Rightarrow R_{i\_2} \leq 0$, mas caso a redução possa ser aplicada tem-se $B_i = 1 \Rightarrow  R_{i\_2} \leq Rmax_{i\_2}$, que corresponde exatamente ao que se pretendia. Esta modificação foi aplicada em todas as restrições dos limites máximos das reduções de tipo 2, para todas as atividades.

Embora a equação \ref{p3:eq2} resolva parte do problema, falta ainda garantir que a variável binária $B_i$ so toma de facto o valor 1 quando a redução do tipo 1 para uma atividade $i$ toma o seu valor máximo. Isso foi conseguido adicionando ao modelo uma restrição para cada atividade que toma a forma da inequação \ref{p3:eq4}.

\begin{equation}
\label{p3:eq4}
R_{i\_1} \geq Rmax_{i\_1} \times B_i
\end{equation}

Note-se como com esta inequação, se $B_i=1$ então $R_{i\_1} \geq Rmax_{i\_1}$. Visto que $R_{i\_1}$ também é limitado (superiormente) pela inequação \ref{p3:eq1}. Tem-se então que $R_{i\_1} \geq Rmax_{i\_1} \wedge R_{i\_1} \leq Rmax_{i\_1} \Rightarrow R_{i\_1} = Rmax_{i\_1} $. Conclui-se por isso que:

\begin{equation}
\label{p3:eq5}
B_i=1 \Rightarrow R_{i\_1} = Rmax_{i\_1}
\end{equation}

Vai-se provar agora que a implicação também é válida no sentido inverso, ou seja $B_i=1 \Leftarrow R_{i\_1} = Rmax_{i\_1}$. Para provar esta implicação vai-se recorrer à prova por redução absurdo. Para tal, suponha-se que existe um valor $R_{i\_1} < Rmax_{i\_1}$ e que em simultâneo $B_i=1$. Facilmente se verifica que tal é impossível, pois caso isso acontecesse, a restrição \ref{p3:eq4} não estaria a ser respeitada -- chegou-se a um absurdo. O absurdo resultou de assumirmos que $B_i=1$, pelo que se pode concluir que $R_{i\_1} < Rmax_{i\_1} \Rightarrow B_i=0$. Por um raciocínio semelhante, também se verifica que se $R_{i\_1} > 0 \Rightarrow B_i=0$. Apenas se consegue ter $B_i=1$ e ao mesmo tempo verificar todas as restrições se $R_{i\_1} = Rmax_{i\_1}$. Assim conclui-se que $R_{i\_1} \neq Rmax_{i\_1} \Leftrightarrow B_i=0$ e que $R_{i\_1} = Rmax_{i\_1} \Leftarrow B_i=1$.


\section{Ficheiro Input}
\label{p3:fichin}

\begin{verbatim}
/*
=== FUNCAO OBJECTIVO ===
Minimizar os custos suplementares de redução das atividades.
*/
min: 400 + 100 R0_2 + 200 R0_1 +
1000 + 300 R1_2 + 600 R1_1 +
300 + 100 R3_2 + 200 R3_1 +
2000 + 400 R4_2 + 800 R4_1 +
1000 + 800 R5_2 + 1600 R5_1 +
800 + 90 R6_2 + 180 R6_1 +
900 + 0 R7_2 + 0 R7_1 +
300 + 0 R9_2 + 0 R9_1 +
1600 + 500 R10_2 + 1000 R10_1 +
1400 + 300 R11_2 + 600 R11_1;


/*
Reducoes maximas
*/
R0_1 <= 0.5;
R0_2 <= 0.5 B0;
R0_1 >= 0.5 B0;

R1_1 <= 1;
R1_2 <= 1 B1;
R1_1 >= 1 B1;

R3_1 <= 0.5;
R3_2 <= 0.5 B3;
R3_1 >= 0.5 B3;

R4_1 <= 2;
R4_2 <= 1 B4;
R4_1 >= 2 B4;

R5_1 <= 0.5;
R5_2 <= 0.5 B5;
R5_1 >= 0.5 B5;

R6_1 <= 1;
R6_2 <= 1 B6;
R6_1 >= 1 B6;

R7_1 <= 0;
R7_2 <= 0 B7;
R7_1 >= 0 B7;

R9_1 <= 0;
R9_2 <= 0 B9;
R9_1 >= 0 B9;

R10_1 <= 0.5;
R10_2 <= 0.5 B10;
R10_1 >= 0.5 B10;

R11_1 <= 1;
R11_2 <= 1 B11;
R11_1 >= 1 B11;


//Nodo Inicial
Tini >= 0 + 0;
//Nodo 0
T0 >= Tini + 0 - Rini;
//Nodo 1
T1 >= T0 + 4 - R0_1 - R0_2;
//Nodo 3
T3 >= T1 + 6 - R1_1 - R1_2;
T3 >= T5 + 4 - R5_1 - R5_2;
T3 >= T4 + 9 - R4_1 - R4_2;
//Nodo 4
T4 >= T0 + 4 - R0_1 - R0_2;
T4 >= T7 + 6 - R7_1 - R7_2;
//Nodo 5
T5 >= T4 + 9 - R4_1 - R4_2;
T5 >= T7 + 6 - R7_1 - R7_2;
T5 >= T10 + 8 - R10_1 - R10_2;
//Nodo 6
T6 >= Tini + 0 - Rini;
//Nodo 7
T7 >= T6 + 5 - R6_1 - R6_2;
//Nodo 9
T9 >= T7 + 6 - R7_1 - R7_2;
T9 >= T11 + 7 - R11_1 - R11_2;
T9 >= T10 + 8 - R10_1 - R10_2;
//Nodo 10
T10 >= T6 + 5 - R6_1 - R6_2;
//Nodo 11
T11 >= T10 + 8 - R10_1 - R10_2;
//Nodo final
Tfim >= T3 + 2 - R3_1 - R3_2;
Tfim >= T5 + 4 - R5_1 - R5_2;
Tfim >= T9 + 2 - R9_1 - R9_2;

/*Forcar reducao de tempo*/
Tfim = 26-4;

bin B1, B3, B4, B5, B6, B7, B9, B10, B11;

\end{verbatim}


\section{Ficheiro Output}

\begin{verbatim}
result	10920
Variables	
B0	0
B1	0
B10	0
B11	0
B3	1
B4	0
B5	0
B6	1
B7	0
B9	0
R0_1	0
R0_2	0
R10_1	0
R10_2	0
R11_1	0
R11_2	0
R1_1	0
R1_2	0
R3_1	0,5
R3_2	0,5
R4_1	1
R4_2	0
R5_1	0
R5_2	0
R6_1	1
R6_2	1
R7_1	0
R7_2	0
R9_1	0
R9_2	0
Rini	0
T0	0
T1	4
T10	3
T11	11
T3	21
T4	9
T5	17
T6	0
T7	3
T9	18
Tfim	22
Tini	0

\end{verbatim}

\newpage
\section{Resultados}

A duração do projeto sem considerar reduções era de 26 unidades de tempo. No modelo forçou-se uma redução de 4 unidades de tempo pela restrição $Tfim = 26-4$, o que reduz a duração do projeto para 22 unidades de tempo. Os resultados do lpsolve mostram que essa redução é possível e obtém-se um custo adicional mínimo reduzindo a duração das seguintes atividades:

\begin{description}
\item[Atividade 3] reduzir 1 unidade (0,5 redução tipo 1 + 0,5 redução tipo 2)

Custo: $300 + 200 \times 0,5 + 100 \times 0,5 = 450$ 
\item[Atividade 4] reduzir 1 unidade (redução tipo 1)

Custo: $2000 + 800 \times 1 = 2800$ 
\item[Atividade 6] reduzir 2 unidades (1 redução tipo 1 + 1 redução tipo 2)

Custo: $800 + 180 \times 1 + 90 \times 1  = 1070$ 
\end{description}

Conforme visto no trabalho 1, a soma dos custos normais das atividades era dado por:
\begin{displaymath}
400+1000+300+2000+1000+800+900+300+1600+1400 = 9700~u.m.
\end{displaymath}

O custo adicional das reduções desta parte é dado por
\begin{displaymath}
150+800+270 = 1220~u.m.
\end{displaymath}

Somando o custo normal das atividades com o custo das reduções tem-se $9700 + 1220 = 10920$, tal como o resultado indicado pelo lpsolve confirma.

A figura \ref{p3:DG} mostra o Diagrama de Gantt correspondente aos resultados:

\begin{figure}[<+htpb+>]
	\centering
	\includegraphics[scale=0.9]{../Imagens/p3_diagrama_gantt}
	\caption{Diagrama de Gantt com reduções às atividades}
	\label{p3:DG}
\end{figure}

\section{Verificação do modelo}

Verifica-se que todas as restrições são respeitadas e que para todas as atividades a redução de tipo 2 só é aplicada quando a redução de tipo 1 da mesma atividade é aplicada ao máximo.
\chapter{Parte 1}
\label{cap:p1}

\section{Determinação da Lista de Atividades}
\begin{figure}[<+htpb+>]
	\centering
	\includegraphics[scale=0.5]{../Imagens/p1_rede_original}
	\caption{Grafo Inicial do enunciado}
	\label{p1:fig:rede_original}
\end{figure}

Antes de partir para a formulação do modelo, foi necessário saber qual a rede a considerar. À rede fornecida no enunciado (figura \ref{p1:fig:rede_original}) foi necessário retirar dois nós, de acordo com a metodologia apresentada na secção \textit{Determinação da Lista de Atividades} presente no final do enunciado. Os números de aluno dos autores deste relatório são 61778, 64282 e 72628. Como o número mais alto é 64282, então D=2 e E=8, sendo por isso os nodos 2 e 8 a ser retirados da rede. A rede resultante da remoção destes dois nós tem a representação gráfica mostrada na figura \ref{p1:fig:rede_com_duracoes}.

\begin{figure}[<+htpb+>]
	\centering
	\includegraphics[scale=0.5]{../Imagens/p1_rede_com_duracoes}
	\caption{Grafo resultante da remoção das atividades 2 e 8, com indicação da duração de cada atividade (em unidades de tempo arbitrárias)}
	\label{p1:fig:rede_com_duracoes}
\end{figure}

\section{Modelo}

\subsection{Parâmetros}

Os parâmetros do problema são a duração de cada atividade e as suas precedências.

\subsection{Variáveis de decisão}
\label{p1:vardec}

Sendo um problema de programação inteira mista, há dois tipos de variáveis neste modelo: inteiras e binárias. As variáveis de decisão inteiras correspondem aquelas que se tinha considerado no trabalho I, ou seja, traduzem o tempo em que cada atividade é iniciada. Assim, a cada atividade está associada uma variável deste tipo. Relativamente ao nome, a opção tomada foi a de considerar $T_{i}$ como o tempo de início da atividade $i$ (em unidades de tempo arbitrárias), em que $i$ corresponde ao número da atividade.

Em relação ao trabalho I, foi necessário introduzir 3 novas variáveis binárias, que genericamente podem ser escritas como $Y_{i\_j}$. Esta variável tomará o valor 1 caso a atividade $i$ se realiza antes da atividade $j$ e 0 caso contrário. O motivo da inclusão destas variáveis será explicitado com mais detalhe na secção \ref{p1:restricoes} quando forem apresentadas as restrições.

\subsection{Função Objetivo}

Neste modelo, quer-se minimizar o tempo de execução total do projeto. Isso corresponde a dizer que queremos que a atividade final seja iniciada o mais cedo possível. A atividade final é na verdade ``fictícia'', pois não corresponde a uma atividade que tenha de ser efetivamente realizada. No entanto para efeitos de modelação, é útil considerá-la, assumindo que é realizada após todas as outras da rede terem terminado e que tem duração de 0 unidades de tempo. Nestas condições, o tempo inicial da atividade final indica a duração do projeto.

Uma vez que a variável $T_{fim}$ indica a duração do projeto, a função objetivo fica simplesmente:

\begin{displaymath} \min~z = T_{fim} \end{displaymath}

\subsection{Restrições}
\label{p1:restricoes}

\subsubsection{Restrições precedências (trabalho I)}
Com as restrições pretende-se indicar o espaço de possíveis soluções. Sabe-se que uma atividade não pode começar sem que as que lhe precedem tenham terminado. Qualquer solução que obedeça a este princípio é uma solução admissível para o problema. Para escrever as restrições é por isso necessário saber quando uma atividade termina. Ora, sabendo que as nossas variáveis de decisão indicam o tempo em que cada atividade se inicia e que temos a duração das mesmas como parâmetro do modelo, podemos dizer que o tempo final de uma atividade corresponde a somar o seu tempo de início com a sua duração. Ou seja:

\begin{displaymath} Tf_{i} = T_{i} + D_{i} \end{displaymath}

Onde:

\begin{itemize} \item[$Tf_{i}$] Tempo em que a atividade $i$ termina
	\item[$T_{i}$] Tempo em que a atividade i começa (variável de decisão)
	\item[$D_{i}$] Duração da atividade $i$ \end{itemize}

Dizer que uma atividade não pode começar sem que as que lhe precedem tenham terminado é o mesmo que dizer que o tempo inicial da atividade tem que ser maior que o tempo final de todas as atividades que lhe precedem. Assumindo que se tem uma atividade $j$ que precede uma atividade $i$, podemos escrever que:

\begin{displaymath} T_{i} \geq T_{j} + D_{j} \end{displaymath}

O modelo terá por isso uma restrição deste tipo por cada nodo e por cada atividade precedente ao nodo. Ou seja, um nó que tenha apenas 1 precedência, apenas originará uma restrição, enquanto que se o nodo tiver por exemplo 3 precedências, dará origem a 3 restrições --- uma restrição para cada precedência do nodo. As restrições completas podem ser consultadas na secção \ref{p1:ficheiro_input}

Visto que os tempos não podem ser negativos, neste modelo tem-se ainda restrições de não-negatividade:

\begin{displaymath} T_{i} \geq 0, \forall i_{\in\{ini,0,1,3,4,5,6,7,9,10,11,fim\}} \end{displaymath}

\subsubsection{Restrições de não-simultaneidade (novas restrições)}
\label{p1:nr}

Embora as restrições do trabalho I modelem bem as restrições de precedências entre as atividades, são insuficientes para lidar com fator extra introduzido neste trabalho de algumas atividades não poderem ser realizadas ao mesmo tempo. Pode-se pensar nestas atividades como atividades que embora não dependam entre si em termos de precedências, dependem entre si por terem que ser feitas numa mesma máquina, por exemplo. Por esse motivo, essas atividades não podem ser feitas em paralelo.

No enunciado é sugerido que se considerem  3 atividades que anteriormente poderiam ser realizadas em paralelo, mas que para efeito deste trabalho prático não o poderão ser. Para verificar que atividades poderiam ser escolhidas, em primeiro lugar analisou-se o Diagrama de Gantt do trabalho I, que se apresenta na figura \ref{p1:DG_T1}.

\begin{figure}[<+htpb+>]
	\centering
	\includegraphics[scale=0.5]{../Imagens/p1_diagrama_gantt_caminho_crit_T1}
	\caption{Diagrama de Gantt do trabalho 1}
	\label{p1:DG_T1}
\end{figure}

Como se pode ver na figura \ref{p1:DG_T1}, as atividades 1, 7 e 10 podem ocorrer em simultâneo no trabalho 1. Foram estas as atividades que se consideraram na resolução do exercício.

Dizer que estas atividades não podem ocorrer em simultâneo corresponde então a assumir que elas são realizadas na mesma máquina, que só suporta 1 atividade ao mesmo tempo. Isto implica dizer que essa máquina irá realizar as atividades segundo uma determinada ordem, que se desconhece à partida. É necessário de alguma forma introduzir no modelo a ideia de que essas atividades são realizadas por uma determinada ordem. A solução encontrada foi a de introduzir 3 novas variáveis binárias do tipo $Yi\_j$ que tomam o valor 1 caso a atividade $i$ se realiza antes da atividade $j$ e 0 caso contrário. As 3 variáveis em concreto do modelo serão $Y1\_7$, $Y7\_10$, $Y1\_10$, que respondem, respectivamente, às questões: ``A atividade 1 realiza-se antes da atividade 7?''; ``A atividade 7 realiza-se antes da atividade 10?'' e ``A atividade 1 realiza-se antes da atividade 10?''. Note-se que com a resposta 1(V) ou 0(F) a estas 3 questões é possível saber a ordem concreta pelas quais as atividades foram realizadas.

Destas 3 atividades, considere-se agora apenas duas delas, por exemplo a atividade 1 e 7. Se a atividade 1 começar antes da atividade 7 e sabendo que a atividade 1 demora 6 unidades de tempo, então tem que se ter uma restrição do tipo $T7 \geq T1 + 6$, para indicar que a atividade 7 so pode começar quando a atividade 1 tiver libertado a máquina. No entanto se condição não se verificar e a atividade 7 começar antes da atividade 1, então a restrição anterior tem que ser ``invalidada'' e o que se tem que verificar será que $T1 \geq T7 + 6$ (a atividade 7 também demora 6 unidades de tempo). A forma encontrada de resolver este problema foi considerar estas duas restrições apresentadas, com a introdução de uma nova parcela em cada uma delas:\\

\begin{equation}
\label{eq:1}
T1 + 6 \leq T7 + M \times (1 - Y1\_7)
\end{equation}
\begin{equation}
\label{eq:2}
T7 + 6 \leq T1 + M \times Y1\_7
\end{equation}

,onde $M$ representa um número arbitrariamente grande.

A ideia fundamental destas restrições é para cada valor da variável binária $Y1\_7$ apenas uma restrição do par seja ativa, sendo a outra restrição ``ignorada'' pelo modelo.
Para verificar que isto acontece veja-se o que acontece quando $Y1\_7$ toma o valor 1, ou seja, a atividade 1 realiza-se antes da 7. Neste caso, a restrição \ref{eq:1} fica simplesmente $T1 + 6 \leq T7$, que garante que o tempo da atividade 7 é superior ao tempo de término da atividade 1. Ainda considerando que $Y1\_7$ toma o valor 1, a restrição \ref{eq:2} pode ser simplificada: $T7 + 6 \leq T1 + M$. Uma vez que o lado direito traduz um número arbitrariamente grande e sendo esta uma restrição de menor ou igual, esta será sempre trivialmente verificada, independentemente do valor de $T7$, que no contexto do problema é sempre de ordem de grandeza inferior a M.
Seguindo um raciocínio semelhante, facilmente se verifica que caso $Y1\_7$ tome o valor 0, ou seja, a atividade 7 se realize antes da 1, passa-se exatamente contrário e é a restrição \ref{eq:2} que fica ativa, sendo a restrição \ref{eq:1} trivialmente verificada.

Recorde-se o problema original de garantir que que as atividades 1,7 e 10 não se realizam ao mesmo tempo. Para garantir isso, basta garantir que para cada par que se pode formar com estas 3 atividades, se tenha 2 restrições semelhantes às apresentadas nos parágrafos anteriores, onde em cada par de restrições figura a variável binária correspondente à ordem entre essas duas atividades. Estas restrições podem ser consultadas na secção \ref{p1:fichin} onde é apresentado o ficheiro de input completo.

\section{Ficheiro Input}
\label{p1:fichin}

Nota: O valor arbitrariamente grande $M$ referido na secção \ref{p1:nr} foi substituído pelo valor 1000 no ficheiro de input e a restrições expandidas, uma vez que o lpsolve não aceita multiplicação entre variáveis nem parêntesis.

\begin{verbatim}
/*
=== FUNCAO OBJECTIVO ===
Minimizar o tempo da atividade final.
*/
min: Tfim;

/*
=== RESTRICOES ===
Para cada no:

TI >= TJ + DJ;
Onde:
TI - Tempo inicial da actividade I
TJ - Tempo inicial da actividade J
DJ - Duracao actividade J
e actividade J precede a actividade I
*/

//Nodo Inicial
Tini >= 0 + 0;
//Nodo 0
T0 >= Tini + 0;
//Nodo 1
T1 >= T0 + 4;
//Nodo 3
T3 >= T1 + 6;
T3 >= T5 + 4;
T3 >= T4 + 9;
//Nodo 4
T4 >= T0 + 4;
T4 >= T7 + 6;
//Nodo 5
T5 >= T4 + 9;
T5 >= T7 + 6;
T5 >= T10 + 8;
//Nodo 6
T6 >= Tini + 0;
//Nodo 7
T7 >= T6 + 5;
//Nodo 9
T9 >= T7 + 6;
T9 >= T11 + 7;
T9 >= T10 + 8;
//Nodo 10
T10 >= T6 + 5;
//Nodo 11
T11 >= T10 + 8;
//Nodo final
Tfim >= T3 + 2;
Tfim >= T5 + 4;
Tfim >= T9 + 2;

//Atividades 1, 7 e 10 nao podem ocorrer
//em simultaneo

//Actividade 1 antes da 7 ?
T1 + 6 <= T7 + 1000 - 1000 Y1_7;
T7 + 6 <= T1 + 1000 Y1_7;

//Atividade 7 antes da 10 ?
T7 + 6 <= T10 + 1000 - 1000 Y7_10;
T10 + 8 <= T7 + 1000 Y7_10;

//Atividade 1 antes da 10 ?
T1 + 6 <= T10 + 1000 - 1000 Y1_10;
T10 + 8 <= T1 + 1000 Y1_10;

bin Y1_7, Y7_10, Y1_10;
\end{verbatim}

\section{Ficheiro Output}

\begin{verbatim}
result	28

Variables	
Tfim	28
Tini	0
T0	0
T1	19
T3	26
T5	22
T4	13
T7	5
T10	11
T6	0
T9	26
T11	19
Y1_7	0
Y7_10	1
Y1_10	0
\end{verbatim}

\newpage
\section{Resultados}
\label{p1:resultados}

O diagrama de Gantt que traduz os resultados obtidos pode ser visto na figura \ref{p1:DG}, onde as atividades 1,7 e 10 foram destacadas a amarelo. 

\begin{figure}[<+htpb+>]
	\centering
	\includegraphics[scale=0.9]{../Imagens/p1_diagrama_gantt}
	\caption{Diagrama de Gantt com novas restrições}
	\label{p1:DG}
\end{figure}

Através do Diagrama de Gantt verifica-se que todas as restrições de precedência continuam a ser respeitadas que as atividades 1, 7 e 10 não estão a ocorrer em simultâneo e são executadas sequencialmente sem intervalos entre elas. O diagrama sugere ainda um tempo mínimo de duração do projeto de 28 unidades de tempo, que está de acordo com o resultado que o lpsolve indica.

Destaca-se o fato da duração do projeto ter aumentado com a imposição das novas restrições. Sem as restrições de simultaneidade o projeto poderia ser feito em 26 unidades de tempo, e agora apenas pode ser feito em 28 unidades de tempo no mínimo. A explicação para este aumento pode ser vista por análise do Diagrama de Gantt. A atividade 7 pertencia ao caminho crítico e começa em T=5. Ora, tanto a atividade 1 como a atividade 10 demoram mais que 5 unidades de tempo a ser realizadas. Isto significa que a atividade 7 tem que ser a primeira das 3 a começar, e só pode começar no mínimo em T=5, pois é precedida pela atividade 6 que demora 5 unidades de tempo. Como a atividade 5 começa em T=5 e leva 6 unidades de tempo a completar, significa que a máquina só pode ser libertada em T=11. A partir desse instante a máquina realiza as atividades 1 e 10 sequencialmente sem intervalos, ficando livre em T=25. No entanto a duração do projeto é maior que 25, pois as atividades que dependem da atividade 10 (suas sucessoras) levam mais 3 unidades de tempo que a duração da atividade 10, formando o que tudo indica ser o novo caminho crítico com a duração de $25+3=28~u.t$.

\section{Verificação do modelo}

Todas as restrições de precedência e de não simultaneidade se verificam. Na secção \ref{p1:resultados} foram apresentados argumentos que ajudam a confirmar que o valor de função objetivo também se encontra correto. O valor das variáveis binárias também está de acordo com a ordem para as atividades no diagrama de Gantt. Sendo que o diagrama sugere que na máquina a ordem das atividades deva ser 7->10->1, então a atividade 1 não começa primeiro que a atividade 7 ($Y1\_7 = 0$), a atividade 7 começa primeiro que a 1 ($Y7\_10=1$) e a atividade 1 não começa primeiro que a 10 ($Y1\_10=0$).



















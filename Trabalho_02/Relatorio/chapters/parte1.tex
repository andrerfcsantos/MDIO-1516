\chapter{Parte 1}
\label{cap:p1}

\section{Determinação da Lista de Atividades}
\begin{figure}[<+htpb+>]
	\centering
	\includegraphics[scale=0.5]{../Imagens/p1_rede_original}
	\caption{Grafo Inicial do enunciado}
	\label{p1:fig:rede_original}
\end{figure}

Antes de partir para a formulação do modelo, foi necessário saber qual a rede a considerar. À rede fornecida no enunciado (figura \ref{p1:fig:rede_original}) foi necessário retirar dois nós, de acordo com a metodologia apresentada na secção \textit{Determinação da Lista de Atividades} presente no final do enunciado. Os números de aluno dos autores deste relatório são 61778 e 64282. Como o número mais alto é 64282, então D=8 e E=2, sendo por isso os nodos 2 e 8 a ser retirados da rede. A rede resultante da remoção destes dois nós tem a representação gráfica mostrada na figura \ref{p1:fig:rede_com_duracoes}.

\begin{figure}[<+htpb+>]
	\centering
	\includegraphics[scale=0.5]{../Imagens/p1_rede_com_duracoes}
	\caption{Grafo resultante da remoção das atividades 2 e 8, com indicação da duração de cada atividade (em unidades de tempo arbitrárias)}
	\label{p1:fig:rede_com_duracoes}
\end{figure}

A resolução do modelo proposto nesta 1ª parte do trabalho envolve a utilização do software \textit{Relax4} que resolve problemas com redes. A indicação dos arcos e vértices ao \textit{Relax4} tem que ser feita de forma muito específica: os vértices têm que estar todos numerados com um número superior a 1 e a numeração dada aos vértices tem que ser sequencial. Tendo isto em consideração, facilmente se vê que usar a nomeação dos vértices proposta pela figura \ref{p1:fig:rede_com_duracoes} causará problemas: as atividades ``Ini'' e ``Fim'' não são números, há uma atividade 0 e dado que algumas atividades foram removidas, mesmo nas atividades que estão numeradas, a numeração não é sequencial. Para resolver isso e antes de se avançar para a resolução do modelo, foi feita uma renomeação dos vértices, conforme consta na figura \ref{p1:fig:p1_rede_renomeada}.

\begin{figure}[<+htpb+>]
	\centering
	\includegraphics[scale=0.5]{../Imagens/p1_rede_renomeada}
	\caption{Grafo resultante da renomeação dos vértices (atividades)}
	\label{p1:fig:p1_rede_renomeada}
\end{figure}

Salvo indicação em contrário, doravante neste relatório sempre que se referir um número de uma atividade, será o número da atividade depois da renomeação, que será diferente do proposto no enunciado.

\section{Modelo}

\subsection{Parâmetros}

Os parâmetros deste modelo são as precedências e as durações de cada atividade.

\subsection{Variáveis de decisão}
\label{p1:subsec:vardec}

O modelo desenvolvido nesta primeira parte tem como objetivo achar o caminho mais longo da rede. Visto que se pretende achar um caminho, cada arco do grafo terá associada uma variável de decisão, que tomará o valor do fluxo que passa nesse arco. No caso particular deste modelo, por motivos que serão detalhados mais à frente (secção \ref{p1:sec:restricoes}), esse fluxo é sempre 0 ou 1 em cada arco, caso esse arco faça ou não parte do caminho mais longo, respetivamente.

Relativamente à nomenclatura das variáveis, usou-se $X_{I\_J}$ para representar a aresta que vai da atividade I para a atividade J. Assim, $X_{2\_4}$ representa a aresta que vai da atividade 2 para a atividade 4.

\subsection{Ofertas e Consumos dos vértices}

Todos os caminhos na rede do problema começam na atividade 1 e terminam na atividade 12. Para resolver o problema do caminho mais longo, assume-se que é injectada uma unidade de fluxo na rede, que a atravessa pelos diferentes nós, até ser consumida no nodo final. Em termos práticos, isto corresponde a dizer que na atividade 1 (inicial) há uma oferta de 1 unidade de fluxo e na atividade 12 (final) essa unidade de fluxo é consumida. O único nó onde há oferta de uma unidade é no nodo inicial e o único nó onde há consumo dessa unidade é no nodo final o que implica que ao longo da rede não são oferecidas unidades de fluxo em nenhum nó e a única unidade de fluxo injetada atravessa o grafo sem ser consumida até chegar ao nó final. Em suma, na atividade 1 é oferecida uma unidade de fluxo, na atividade 12 essa unidade de fluxo é consumida e nas restantes atividades não há oferta nem consumo.


\subsection{Custos dos arcos e função objetivo}

Como se pretende achar o caminho mais longo, a função objetivo terá que ser uma expressão que indique a duração de um caminho, que queremos que tome o maior valor possível. Trata-se por isso de um problema de maximização.

Visto que os valores das variáveis de decisão indicam os arcos que fazem ou não parte de um caminho, para se construir a função objetivo com essas variáveis, foi necessário associar um custo a cada um dos arcos. A decisão tomada foi a de assumir que cada arco tem um custo correspondente à duração da atividade de onde esse arco é originado. Por exemplo, o arco $X_{2\_3}$ tem origem na atividade 2 e destino na atividade 3, e terá um custo de 4, visto ser essa a duração da atividade 2. O sentido em termos práticos de assumir que a duração não está na atividade mas sim nas arestas do grafo que dele saem  corresponde a dizer que a atividade em si não tem duração, o que tem duração é sim a passagem dessa atividade para uma outra. Embora isto possa ser pouco intuitivo em termos reais, esta consideração revela-se extremamente útil para a resolução do modelo como veremos de seguida.

Em qualquer solução admissível, o custo de um arco só deverá ter influencia no valor da função objetivo se o arco fizer parte do caminho. Uma vez que as variáveis de decisão traduzem com 1 ou 0 o facto de o arco fazer ou não parte do caminho, então para saber o custo efetivo de um arco numa determinada solução, basta multiplicar o seu custo pela variável de decisão associada. Designando por $C_{I}$ esse custo efetivo, temos a seguinte expressão:

\begin{displaymath}
C_{I} = C_{I\_J} \times X_{I\_J}
\end{displaymath}

Onde:
\begin{description}
	\item[$C_{I\_J}$] Custo associado ao arco que vai de I para J - parâmetro do problema
	\item[$X_{I\_J}$] Variável de decisão indicativa se o arco faz ou não parte do caminho, conforme detalhado na secção~\ref{p1:subsec:vardec}.
\end{description}

Uma vez que tanto as variáveis de decisão como as durações das atividades estão associadas aos arcos. Esta multiplicação toma o valor do custo do arco ou zero, caso o arco faça ou não parte do caminho crítico, respectivamente. Este resultado mostra a utilidade em termos de resolução de modelo de anteriormente se ter associado a duração das atividades aos arcos.

A função objetivo será o somatório de todos esses custos. Em termos genéricos, pode ser escrita como:

\begin{displaymath}
\max~z = \sum C_{I}
\end{displaymath}

Expandindo a expressão e substituindo os valores de $C_{I\_J}$ pelos valores de custos do enunciado, juntamente com as variáveis de decisão, temos a seguinte expressão:

\begin{Verbatim}
max: 0 X1_2 + 0 X1_7 + 4 X2_3 + 4 X2_5 + 6 X3_4 + 2 X4_12 + 
9 X5_4 + 9 X5_6 + 4 X6_4 + 4 X6_12 + 5 X7_8 + 5 X7_10 + 
6 X8_5 + 6 X8_6 + 6 X8_9 + 2 X9_12 + 8 X10_6 + 8 X10_9 + 
8 X10_11 + 7 X11_9
\end{Verbatim}

\subsection{Restrições}
\label{p1:sec:restricoes}

\subsubsection{Conservação de fluxo}
Com as restrições pretende-se indicar o espaço de possíveis soluções. Qualquer caminho no grafo corresponde a uma solução admissível do problema. A forma encontrada de representar um caminho em termos de restrições, foi a de assumir que em cada nó o $fluxo~de~entrada = fluxo~de~saida$. Com esta restrição, se nada entrar num nó, então nada sairá, o que corresponde a dizer que o nó não faz parte do caminho. Por outro lado, se alguma coisa entrar, ou seja, se o nó fizer parte do caminho, então queremos que o mesmo fluxo saia do nó. Isto corresponde a dizer que um caminho tem que começar e terminar, o fluxo não pode ficar ``parado'' no meio da rede. 

Embora sejam um bom ponto de partida, as restrições de conservação do fluxo só por si são insuficientes, uma vez que permitem soluções em que entrem num nó 2 ou mais unidades de fluxo, e que as mesmas unidades saiam. Tais soluções não são admissíveis no modelo, pois não traduzem um caminho. Para resolver este problema foi necessário assumir que se injeta na rede 1 unidade de fluxo no nodo inicial, e que essa unidade de fluxo sai no nodo final. A injeção de uma unidade de fluxo, em conjunto com as restrições de conservação do fluxo resolvem o problema, uma vez que se entrar uma unidade de fluxo num nó, então essa unidade (e só essa) também sairá do nó. Por outras palavras, tem-se a definição de um caminho.

As restrições do modelo correspondem por isso a dizer que em cada nó:

\begin{displaymath}
fluxo~entrada = fluxo~saida \Leftrightarrow  fluxo~entrada - fluxo~saida = 0
\end{displaymath}

Ao ter a equação escrita da segunda forma, considera-se implicitamente o fluxo de entrada como sendo positivo e o fluxo de saída como negativo. 

As variáveis do modelo irão tomar apenas o valor de 0 ou 1. A restrição é implícita ao modelo pois como se está a injetar uma unidade de fluxo num nó e ao mesmo tempo a dizer que o que entra em cada nó tem que ser o mesmo que sai, então todas as variáveis forçosamente terão apenas o valor de 0 ou 1. Para a rede que se está a tratar, as restrições de conservação de fluxo são:

\begin{verbatim}
//Nodo 1
1 - X1_2 - X1_7 =0;
//Nodo 2
X1_2 - X2_3 - X2_5 =0;
//Nodo 3
X2_3 - X3_4 =0;
//Nodo 4
X3_4 + X5_4 + X6_4 - X4_12 =0;
//Nodo 5
X2_5 + X8_5 - X5_4 - X5_6 =0;
//Nodo 6
X5_6 + X8_6 + X10_6 - X6_4 - X6_12 =0;
//Nodo 7
X1_7 - X7_8 - X7_10 =0;
//Nodo 8
X7_8 - X8_5 - X8_6 - X8_9 =0;
//Nodo 9
X8_9 + X10_9 + X11_9 - X9_12 =0;
//Nodo 10
X7_10 - X10_6 - X10_9 - X10_11 =0;
//Nodo 11
X10_11 - X11_9 =0;
//Nodo 12
X4_12 + X6_12 + X9_12 - 1 =0;
\end{verbatim}

\subsubsection{Limites superiores}

A capacidade em cada arco é ilimitada. Embora neste modelo em cada arco apenas passe 1 unidade de fluxo no máximo, tal deve-se apenas ao facto de também ser injetada apenas uma unidade de fluxo no inicio da rede, não sendo uma propriedade dos arcos em si. Ou seja:

\begin{displaymath}
X_{I\_J} \geq 0, \text{\small para todos os pares (I,J) arcos do grafo}
\end{displaymath}


\subsection{Modelo Final}

As considerações do modelo feitas anteriormente estão representadas na figura \ref{p1:fig:p1_redeComCap}.

\begin{figure}[<+htpb+>]
	\centering
	\includegraphics[scale=0.5]{../Imagens/p1_redeComCap}
	\caption{Rede com indicação dos custos e limites superiores dos arcos}
	\label{p1:fig:p1_redeComCap}
\end{figure}


\section{Ficheiro Input}

\subsection{Ajustes ao modelo}

Antes de escrever o ficheiro de input passado ao \textit{Relax4} foi necessário efetuar primeiro algumas alterações ao modelo, que aqui se explicam.

Em primeiro lugar, o problema do caminho mais longo é um problema de maximização, no entanto o \textit{Relax4} apenas resolve problemas de minimização. A solução encontrada para resolver isto foi passar a função objectivo para o seu simétrico. Desta forma o problema de maximização torna-se num problema equivalente de minimização. A nova função objetivo considerada é:

\begin{Verbatim}
min: - 0 X1_2 - 0 X1_7 - 4 X2_3 - 4 X2_5 - 6 X3_4 - 2 X4_12 - 
9 X5_4 - 9 X5_6 - 4 X6_4 - 4 X6_12 - 5 X7_8 - 5 X7_10 - 
6 X8_5 - 6 X8_6 - 6 X8_9 - 2 X9_12 - 8 X10_6 - 8 X10_9 - 
8 X10_11 - 7 X11_9
\end{Verbatim}

Em termos práticos, esta mudança corresponde a dizer que os arcos passam ter custos negativos.

Em segundo lugar, o \textit{Relax4} exige a indicação dos limites superiores dos seus arcos. Como referido anteriormente, neste modelo considera-se que os arcos têm capacidade infinita. Como o relax não tem nenhuma notação para representar infinito, foi considerado um valor muito grande (valor 1000) que se sabe que nunca é atingido no modelo.

\subsection{Ficheiro Input}

O ficheiro de input dado ao \textit{Relax4} foi o seguinte:

\begin{verbatim}
12
20
1 2 0 1000
1 7 0 1000
2 3 -4 1000
2 5 -4 1000
3 4 -6 1000
4 12 -2 1000
5 4 -9 1000
5 6 -9 1000
6 4 -4 1000
6 12 -4 1000
7 8 -5 1000
7 10 -5 1000
8 5 -6 1000
8 6 -6 1000
8 9 -6 1000
9 12 -2 1000
10 6 -8 1000
10 9 -8 1000
10 11 -8 1000
11 9 -7 1000
1
0
0
0
0
0
0
0
0
0
0
-1
\end{verbatim}

\section{Ficheiro Output}

\begin{verbatim}
 END OF READING
 NUMBER OF NODES = 12, NUMBER OF ARCS = 20
 CONSTRUCT LINKED LISTS FOR THE PROBLEM
 CALLING RELAX4 TO SOLVE THE PROBLEM
 ***********************************
 TOTAL SOLUTION TIME =  0. SECS.
 TIME IN INITIALIZATION =  0. SECS.
 1 7  1.
 4 12  1.
 5 6  1.
 6 4  1.
 7 8  1.
 8 5  1.
 OPTIMAL COST =  -26.
 NUMBER OF AUCTION/SHORTEST PATH ITERATIONS = 37
 NUMBER OF ITERATIONS =  11
 NUMBER OF MULTINODE ITERATIONS =  1
 NUMBER OF MULTINODE ASCENT STEPS =  0
 NUMBER OF REGULAR AUGMENTATIONS =  1
 ***********************************
\end{verbatim}

\section{Interpretação do resultado}

O output do \textit{Relax4} mostra que o caminho mais longo passa pelos arcos (1,7), (4,12), (5,6), (6,4), (7,8) e (8,5). É possível ainda verificar que em cada um destes arcos passa uma unidade de fluxo, tal como seria de esperar.

Para a função objectivo, é indicado o valor de -26 unidades de tempo. Visto que foi considerado o simétrico da função objectivo inicial para transformar o problema de maximização num problema de minimização, este resultado só tem sentido se se considerar o seu simétrico. Tem-se assim que a duração do caminho mais longo é de 26 unidades de tempo.

Este resultado está correto, uma vez que foi também este o caminho e valor de função objectivo que se obteve no Trabalho 1. Na figura \ref{p1:fig:p1_redeCamCrit} encontra-se representada a rede com indicação dos vértices pertencentes ao caminho crítico a verde e os arcos a vermelho.

\begin{figure}[<+htpb+>]
	\centering
	\includegraphics[scale=0.7]{../Imagens/p1_redeCamCrit}
	\caption{Rede resultante com indicação do caminho crítico}
	\label{p1:fig:p1_redeCamCrit}
\end{figure}


























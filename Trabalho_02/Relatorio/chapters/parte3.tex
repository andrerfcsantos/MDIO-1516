\chapter{Parte 3}
\label{cap:p3}


\section{Rede Geral - \textit{just-in-time} entre atividades 1 e 2}

Para verificar a alteração de tempo de início da atividade 1 com e sem a restrição de \textit{just-in-time} para a resolução deste exercício foram construídos dois modelos para a rede geral do enunciado: o modelo de caminho mais longo e o modelo dos tempos de início das atividades (com e sem a restrição de \textit{just-in-time}). Uma vez que estes modelos são em tudo semelhantes aos efetuados no trabalho 1 para a rede do grupo e por não serem o foco principal do exercício, nesta secção vai-se apenas referir os resultados importantes que se retiraram da resolução desses modelos.

No modelo dos tempos de início das atividades, a única restrição que relaciona as atividades 1 e 2 é $T_2 \geq T_1 + 6 \Leftrightarrow T_2 - T_1 \geq 6$, que indica que o tempo em que se inicia a atividade 2 tem que ser maior que o tempo em que a atividade 1 acaba. A restrição de just-in-time entre a atividade 1 e 2 fica $T_2 \le T_1 + 6 \Leftrightarrow T_1 - T_2~\geq -6$. Se juntarmos esta restrição com a restrição apresentada no parágrafo anterior, conclui-se que $T_2 = T_1 + 6$.

A figura \ref{p3:fig:p3_ganttGeralSemRes} mostra o Diagrama de Gantt que resulta da resolução do modelo sem qualquer restrição de \textit{just-in-time}. As barras a vermelho representam as atividades do caminho crítico.

\begin{figure}[<+htpb+>]
	\centering
	\includegraphics[scale=0.7]{../Imagens/p3_ganttGeralSemRes}
	\caption{Diagrama de Gantt para a rede geral, sem restrição de \textit{just-in-time}}
	\label{p3:fig:p3_ganttGeralSemRes}
\end{figure}

Como se pode ver, sem qualquer restrição de \textit{just-in-time}, a atividade 1 começa no instante 4 e a atividade 2 começa no instante 20.

\newpage

A figura \ref{p3:fig:p3_ganttGeralComRes} mostra o mesmo diagrama, mas com a restrição de \textit{just-in-time} adicionada entre as atividades 1 e 2.

\begin{figure}[<+htpb+>]
	\centering
	\includegraphics[scale=0.7]{../Imagens/p3_ganttGeralComRes}
	\caption{Diagrama de Gantt para a rede geral, com restrição de \textit{just-in-time} entre as atividades 1 e 2}
	\label{p3:fig:p3_ganttGeralComRes}
\end{figure}

Quando é adicionada a restrição de \textit{just-in-time} a atividade 1 passa a ter início no instante 14, de forma a acabar precisamente quando a atividade 2 se inicia.

A explicação para este resultado é simples. Anteriormente viu-se que adicionar a restrição de just-in-time ao modelo significava impor a restrição $T_2 = T_1 + 6$, que basicamente força a que o tempo de início da atividade 2 seja imediatamente a seguir à atividade 1 ter terminado. A atividade 2 pertence ao caminho crítico logo não pode começar mais cedo. O instante 20 é o instante mais cedo em que a atividade 2 pode começar. A atividade 1 não pertence ao caminho crítico, portanto pode começar mais tarde. Analiticamente, calcular o tempo de início da atividade 1 corresponde portanto a resolver a equação $20 = T_1 + 6$ de onde se tira $T_1 = 20 - 6 = 14$. Assim, após a adição da restrição, a atividade 1 inicia-se no instante 14, tal como o Diagrama de Gantt da figura \ref{p3:fig:p3_ganttGeralComRes} sugere.

\section{Rede do grupo - \textit{just-in-time}}

As atividades escolhidas na rede do grupo entre as quais se adicionou um restrição de \textit{just-in-time} foram as atividades 3 e 4.

Antes da adição da restrição \textit{just-in-time}, a solução do modelo pode ser representada no Diagrama de Gantt da figura \ref{p3:fig:p3_ganttGrupoSemRes}.

\begin{figure}[<+htpb+>]
	\centering
	\includegraphics[scale=0.7]{../Imagens/p3_ganttGrupoSemRes}
	\caption{Diagrama de Gantt para a rede do grupo, sem restrição de \textit{just-in-time}}
	\label{p3:fig:p3_ganttGrupoSemRes}
\end{figure}

A atividade 4 pertence ao caminho crítico e começa no instante 24, enquanto que a atividade 3 começa no instante 4. A restrição de \textit{just-in-time} adicionada é $T_4 \le T_3 + 6 \Leftrightarrow T_3 - T_4 \geq -6$. Se juntarmos esta restrição com a restrição $T_4 - T_3 \geq 6$ já existente no modelo, está-se a forçar que $T_4 = T_3 + 6$. A atividade 4 pertence ao caminho crítico logo não pode começar mais cedo. O instante 24 é o instante mais cedo em que a atividade 4 pode começar. A atividade 3 não pertence ao caminho crítico, portanto pode começar mais tarde. Analiticamente, calcular o tempo de início da atividade 1 corresponde portanto a resolver a equação $24 = T_3 + 6$ de onde se tira $T_3 = 24 - 6 = 18$, tal como o Diagrama de Gantt da figura \ref{p3:fig:p3_ganttGrupoComRes} sugere.

\begin{figure}[<+htpb+>]
	\centering
	\includegraphics[scale=0.7]{../Imagens/p3_ganttGrupoComRes}
	\caption{Diagrama de Gantt para a rede do grupo, com restrição de \textit{just-in-time}}
	\label{p3:fig:p3_ganttGrupoComRes}
\end{figure}


\subsection{Restrição \textit{just-in-time} e modelo dual}

O modelo dos tempos de início das atividades é um problema de minimização com restrições de $\geq$. Para mostrar que a restrição \textit{just-in-time} entre as atividades 3 e 4 corresponde a um arco do modelo dual, é útil colocar a restrição $T_4 \le T_3 + 6$ na forma canónica: $T_3 - T_4 \geq -6$. Esta restrição, no quadro simplex do modelo vai-se traduzir numa linha apenas com zeros, excepto na coluna da variável $T_3$, que terá o valor 1 e na coluna de $T_4$ que terá o valor -1. Tal como as transformações feitas na secção \ref{p2:dual}, no modelo dual, estes valores irão ficar numa coluna correspondente a um arco e os valores de +1 e -1 indicam o destino e origem do arco e o valor do lado direito da inequação indica o custo do novo arco. Assim, têm-se que o novo arco criado vai da atividade 4 para a atividade 3 e tem um custo negativo de -6.

\subsection{Ficheiro input com novo arco}

O novo arco adicionado é \text{4 3 6 1000}. Embora o custo do novo arco seja negativo, visto estarmos a resolver um problema de maximização num software de minimização, a função objectivo tem que ser invertida, dai se ter o valor 6 e não -6 no custo do novo arco.


\begin{Verbatim}
12
21
1 2 0 1000
1 7 0 1000
2 3 -4 1000
2 5 -4 1000
3 4 -6 1000
4 3 6 1000
4 12 -2 1000
5 4 -9 1000
5 6 -9 1000
6 4 -4 1000
6 12 -4 1000
7 8 -5 1000
7 10 -5 1000
8 5 -6 1000
8 6 -6 1000
8 9 -6 1000
9 12 -2 1000
10 6 -8 1000
10 9 -8 1000
10 11 -8 1000
11 9 -7 1000
1
0
0
0
0
0
0
0
0
0
0
-1
\end{Verbatim}

\subsection{Ficheiro Output}

De seguida apresenta-se o ficheiro output obtido.

\begin{Verbatim}
 END OF READING
 NUMBER OF NODES = 12, NUMBER OF ARCS = 21
 CONSTRUCT LINKED LISTS FOR THE PROBLEM
 CALLING RELAX4 TO SOLVE THE PROBLEM
 ***********************************
 TOTAL SOLUTION TIME =  0. SECS.
 TIME IN INITIALIZATION =  0. SECS.
 1 7  1.
 3 4  1000.
 4 3  1000.
 4 12  1.
 5 6  1.
 6 4  1.
 7 8  1.
 8 5  1.
 OPTIMAL COST =  -26.
 NUMBER OF AUCTION/SHORTEST PATH ITERATIONS = 55
 NUMBER OF ITERATIONS =  7
 NUMBER OF MULTINODE ITERATIONS =  1
 NUMBER OF MULTINODE ASCENT STEPS =  1
 NUMBER OF REGULAR AUGMENTATIONS =  1
 ***********************************
\end{Verbatim}

\subsection{Discussão de Resultados}
\label{p3:discRes}

O caminho crítico não se alterou pela introdução de uma restrição just-in-time entre as atividades 3 e 4. O caminho mais longo até à segunda atividade (atividade 4) também se manteve, o que seria de esperar, visto que o caminho crítico se manteve e a atividade 4 pertencia ao caminho crítico antes da introdução do novo arco.

Há ainda que referir o transporte de 1000 unidades do arco 3 para 4 e do arco 4 para 3. Visto que apenas entra uma unidade de fluxo no circuito, estes valores têm que ser vistos como um circuito que a unidade de fluxo faz entre os vértices 3 e 4, 1000 vezes. Estes arcos têm custos simétricos o que significa que um circuito entre os vértices 3 e 4 em que a unidade de fluxo percorre o arco de 3 para 4 o mesmo número de vezes que percorre o arco de 4 para 3 é equivalente a uma solução em que não há fluxo entre os vértices 3 e 4. O valor da função objetivo mantém-se.


